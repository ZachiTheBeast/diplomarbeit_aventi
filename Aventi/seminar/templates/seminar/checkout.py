import urllib
import urllib.parse
import requests
from requests.auth import HTTPBasicAuth

post_data = {"purchase_country": "AT", "purchase_currency": "EUR", "locale": "de-AT", "merchant_urls": {"terms": "https://www.example.com/terms.html", "checkout": "http://127.0.0.1:8000/chateau-gaillard-30-burg-und-kirche/checkout", "confirmation": "http://127.0.0.1:8000/chateau-gaillard-30-burg-und-kirche/confirmation", "push": "https://www.example.com/api/push"}, "order_amount": "2200", "order_tax_amount": 0, "order_lines": [{"reference": "Exkursion", "name": "Einkaufsbummel", "quantity": 1, "quantity_unit": "pcs", "unit_price": "1000", "tax_rate": 0, "total_amount": "1000", "total_discount_amount": 0, "total_tax_amount": 0}, {"reference": "Mittagessen", "name": "Ja", "quantity": 1, "quantity_unit": "pcs", "unit_price": "1200", "tax_rate": 0, "total_amount": "1200", "total_discount_amount": 0, "total_tax_amount": 0}]}
#headers = {'PK14387_7c7072b4680b': 'y110mtQMslO3Jqa5'}
#data = urllib.parse.urlencode(post_data)
#data = data.encode('ascii')
#req = urllib.request.Request('https://api.playground.klarna.com/checkout/v3/orders', data, headers, method='POST')
#result = urllib.request.urlopen(req)
#print(result.read())

url = 'https://api.playground.klarna.com/checkout/v3/orders'
response = requests.post(url, json=post_data, auth=HTTPBasicAuth('PK14387_7c7072b4680b', 'y110mtQMslO3Jqa5'))
data = response.content
print(data)