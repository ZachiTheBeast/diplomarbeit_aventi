from django.contrib import admin
from .models import Post, Description, Registration, Arrival, ProgramDay, Item, Order, Component, Order_Component, Post_ProgramDay, Price

admin.site.register(Post)
admin.site.register(Description)
admin.site.register(Registration)
admin.site.register(Arrival)
admin.site.register(ProgramDay)
admin.site.register(Item)
admin.site.register(Order)
admin.site.register(Component)
admin.site.register(Price)
admin.site.register(Order_Component)
admin.site.register(Post_ProgramDay)