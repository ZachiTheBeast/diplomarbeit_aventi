from django.shortcuts import render, get_object_or_404, redirect, get_list_or_404, render_to_response
from base64 import urlsafe_b64encode, urlsafe_b64decode
import base64
from Crypto.Cipher import AES
from Crypto import Random
from .models import Post, Item, Order, Component, Order_Component, ProgramDay, Registration, Post_ProgramDay, User
from django.views.generic import View
import codecs
from django.views.generic.detail import DetailView
from django.utils import timezone
from django.template.loader import get_template
from django.http import HttpResponse, HttpResponseRedirect
from io import BytesIO
from django.template import Context
from cgi import escape
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.mixins import LoginRequiredMixin
import json
import urllib
import subprocess
import requests
from requests.auth import HTTPBasicAuth
import re
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model
from users.models import CustomUser
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
import xlwt

def home_view(request):
    context = {
        'posts': Post.objects.all()
    }
    return render(request, 'seminar/home.html', context)

def home_fr_view(request):
    context = {
        'posts': Post.objects.all()
    }
    return render(request, 'seminar/home_fr.html', context)

def home_en_view(request):
    context = {
        'posts': Post.objects.all()
    }
    return render(request, 'seminar/home_en.html', context)



def about_view(request):
    return render(request, 'seminar/about.html', {'title': 'About'})
    
def about_view_fr(request):
    return render(request, 'seminar/about_fr.html', {'title': 'About'})

def about_view_en(request):
    return render(request, 'seminar/about_en.html', {'title': 'About'})

def imprint_view(request):
    return render(request, 'seminar/imprint.html', {'title': 'Imprint'})

def imprint_view_fr(request):
    return render(request, 'seminar/imprint_fr.html', {'title': 'Imprint'})

def imprint_view_en(request):
    return render(request, 'seminar/imprint_en.html', {'title': 'Imprint'})

def privacypolicy_view(request):
    return render(request, 'seminar/privacypolicy.html', {'title': 'Privacypolicy'})

def privacypolicy_view_fr(request):
    return render(request, 'seminar/privacypolicy_fr.html', {'title': 'Privacypolicy-fr'})

def privacypolicy_view_en(request):
    return render(request, 'seminar/privacypolicy_en.html', {'title': 'Privacypolicy-en'})

def contact_view(request):
    return render(request, 'seminar/contact.html', {'title': 'Contact'})

def post_detail_view(request, slug):
    test = 0
    post = get_object_or_404(Post, slug = slug)
    days_count = 0
    days = []
    try:
        post_days = get_list_or_404(Post_ProgramDay, post=post)
        days_count = 1
        for day in post_days:
            days.append(day.programday)
    except:
        pass 
    registration = get_object_or_404(Registration, related_post=post)
    grouplist = []
    for g in request.user.groups.all():
        grouplist.append(g)
    return render(request, 'seminar/post.html', {'Post': post, 'groups': grouplist, 'days':days, 'registration': registration, 'days_counter': days_count, 'test': test})

def post_detail_view_fr(request, slug):
    post = get_object_or_404(Post, slug = slug)
    days_count = 0
    try:
        post_days = get_list_or_404(Post_ProgramDay, post=post)
        days_count = 1
        days = []
        for day in post_days:
            days.append(day.programday)
    except:
        pass 
    registration = get_object_or_404(Registration, related_post=post)
    grouplist = []
    for g in request.user.groups.all():
        grouplist.append(g)
    return render(request, 'seminar/post_fr.html', {'Post': post, 'groups': grouplist, 'days':days, 'registration': registration, 'days_counter': days_count})

def post_detail_view_en(request, slug):
    post = get_object_or_404(Post, slug = slug)
    days_count = 0
    try:
        post_days = get_list_or_404(Post_ProgramDay, post=post)
        days_count = 1
        days = []
        for day in post_days:
            days.append(day.programday)
    except:
        pass 
    registration = get_object_or_404(Registration, related_post=post)
    grouplist = []
    for g in request.user.groups.all():
        grouplist.append(g)
    return render(request, 'seminar/post_en.html', {'Post': post, 'groups': grouplist, 'days':days, 'registration': registration, 'days_counter': days_count})



def form_view_fr(request, slug):
    post = get_object_or_404(Post, slug=slug)
    group_access = False

    for g in request.user.groups.all():
        if g.name == "is_authorized":
            group_access = True

    if group_access:
        items = Item.objects.filter(post=post).order_by('sequence')
        itemscounter = Item.objects.filter(post=post).count()
        components_for_post=[]
        delete_items_from_list =[]
        counter = 1
        try:
            order = get_object_or_404(Order, user=request.user)
            order.delete()
            return render(request, 'seminar/form_fr.html', {'components': components_for_post, 'items': items, 'post': post, 'counted_items': itemscounter})

        except:
            return render(request, 'seminar/form_fr.html', {'components': components_for_post, 'items': items, 'post': post, 'counted_items': itemscounter})
    else:
        return post_detail_view_fr(request, slug)

def form_view_en(request, slug):
    post = get_object_or_404(Post, slug=slug)
    group_access = False

    for g in request.user.groups.all():
        if g.name == "is_authorized":
            group_access = True

    if group_access:
        items = Item.objects.filter(post=post).order_by('sequence')
        itemscounter = Item.objects.filter(post=post).count()
        components_for_post=[]
        delete_items_from_list =[]
        counter = 1

        try:
            order = get_object_or_404(Order, user=request.user)
            order.delete()
            return render(request, 'seminar/form_en.html', {'components': components_for_post, 'items': items, 'post': post, 'counted_items': itemscounter})

        except:
            return render(request, 'seminar/form_en.html', {'components': components_for_post, 'items': items, 'post': post, 'counted_items': itemscounter})
    else:
        return post_detail_view_en(request, slug)

def form_view(request, slug):
    post = get_object_or_404(Post, slug=slug)
    group_access = False

    for g in request.user.groups.all():
        if g.name == "is_authorized":
            group_access = True

    if group_access:
        items = Item.objects.filter(post=post).order_by('sequence')
        itemscounter = Item.objects.filter(post=post).count()
        components_for_post=[]
        delete_items_from_list =[]
        counter = 1

        try:
            order = get_object_or_404(Order, user=request.user)
            order.delete()
            return render(request, 'seminar/form.html', {'components': components_for_post, 'items': items, 'post': post, 'counted_items': itemscounter})

        except:
            return render(request, 'seminar/form.html', {'components': components_for_post, 'items': items, 'post': post, 'counted_items': itemscounter})
    else:
        return post_detail_view(request, slug)

def order_summary_view(request, slug):
    post = get_object_or_404(Post, slug=slug)
    group_access = False

    for g in request.user.groups.all():
        if g.name == "is_authorized":
            group_access = True

    if group_access:
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        component_names = body['components']
        item_names = body['items']
        checked_components = []
        items_length = len(item_names)
        
        for i in range(items_length):
            components = get_list_or_404(Component, item = get_object_or_404(Item, name=item_names[i]))
            for component in components:
                if component.name == component_names[i]:
                    checked_components.append(component)
                    break
                else:
                    pass

    
        try:
            order = get_object_or_404(Order, user=request.user)
            return render(request, 'seminar/home.html')
        except:
            order_date = timezone.now()
            order = Order.objects.create(user=request.user, ordered_date= order_date)
            order.save()

            for component in checked_components:
                order_component = Order_Component()
                order_component.order = order
                order_component.component = component
                order_component.save()

        return HttpResponse('OK')
    else:
        return HttpResponse('Error', content_type='text/plain')


def order_summary_view_data(request, slug):
    order = get_object_or_404(Order, user=request.user)
    post = get_object_or_404(Post, slug=slug)
    group_access = False

    for g in request.user.groups.all():
        if g.name == "is_authorized":
            group_access = True

    if group_access:
        try:
            components_for_order = get_list_or_404(Order_Component, order=order)
        except:
            return form_view(request, slug)
        
        return render(request, 'seminar/summary.html', {'order': order, 'order_component': components_for_order, 'post': post})

def order_summary_view_data_fr(request, slug):
    order = get_object_or_404(Order, user=request.user)
    post = get_object_or_404(Post, slug=slug)
    group_access = False

    for g in request.user.groups.all():
        if g.name == "is_authorized":
            group_access = True

    if group_access:
        try:
            components_for_order = get_list_or_404(Order_Component, order=order)
        except:
            return form_view(request, slug)
        return render(request, 'seminar/summary_fr.html', {'order': order, 'order_component': components_for_order, 'post': post})

def order_summary_view_data_en(request, slug):
    order = get_object_or_404(Order, user=request.user)
    post = get_object_or_404(Post, slug=slug)
    group_access = False

    for g in request.user.groups.all():
        if g.name == "is_authorized":
            group_access = True

    if group_access:
        try:
            components_for_order = get_list_or_404(Order_Component, order=order)
        except:
            return form_view(request, slug)
        return render(request, 'seminar/summary_en.html', {'order': order, 'order_component': components_for_order, 'post': post})


def checkout_view_klarna(request, slug):
    order = get_object_or_404(Order, user=request.user)
    post = get_object_or_404(Post, slug=slug)
    group_access = False

    for g in request.user.groups.all():
        if g.name == "is_authorized":
            group_access = True

    if group_access:
        components_for_order = get_list_or_404(Order_Component, order=order)

        total_taxes = 0
        total = 0


        post_data = {
            "purchase_country": "AT",
            "purchase_currency": "EUR",
            "locale": "de-AT",
            "merchant_urls": {
            "terms": "https://www.example.com/terms.html",
            "checkout": "http://chateaugaillard2020.imareal.sbg.ac.at/"+slug+"/checkout",
            "confirmation": "http://chateaugaillard2020.imareal.sbg.ac.at/"+slug+"/confirmation",
            "push": "https://www.example.com/api/push"
            },
        }
        post_data_orderline = []
        for order_component in components_for_order:
            tax_amount = order_component.component.item.tax_amount * order_component.component.price / 100
            taxed_price = order_component.component.price + tax_amount
            total += taxed_price
            total_taxes += tax_amount

            tax_amount = round(tax_amount, 3)

            price = re.sub('[.]', '', str(taxed_price))+"0"
            tax_rate = re.sub('[.]', '', str(order_component.component.item.tax_amount))+"0"
            tax_amount = re.sub('[.]', '', str(tax_amount))+"0"

            new_orderline = { 
                "name": order_component.component.item.name,
                "quantity": 1,
                "quantity_unit": "pcs",
                "unit_price": price,
                "tax_rate": tax_rate,
                "total_amount": price,
                "total_discount_amount": 0,
                "total_tax_amount": tax_amount
            }
            post_data_orderline.append(new_orderline)

        total = round(total, 3)
        total_taxes = round(total_taxes, 3)

        total = re.sub('[.]', '', str(total))+"0"
        total_taxes = re.sub('[.]', '', str(total_taxes))+"0"

        post_data["order_amount"] = total
        post_data["order_tax_amount"] = total_taxes
        post_data["order_lines"] = post_data_orderline
    


        #post_data = {"purchase_country": "AT", "purchase_currency": "EUR", "locale": "de-AT", "merchant_urls": {"terms": "https://www.example.com/terms.html", "checkout": "http://127.0.0.1:8000/chateau-gaillard-30-burg-und-kirche/checkout", "confirmation": "http://127.0.0.1:8000/chateau-gaillard-30-burg-und-kirche/confirmation", "push": "https://www.example.com/api/push"}, "order_amount": "2200", "order_tax_amount": 0, "order_lines": [{"reference": "Exkursion", "name": "Einkaufsbummel", "quantity": 1, "quantity_unit": "pcs", "unit_price": "1000", "tax_rate": 0, "total_amount": "1000", "total_discount_amount": 0, "total_tax_amount": 0}, {"reference": "Mittagessen", "name": "Ja", "quantity": 1, "quantity_unit": "pcs", "unit_price": "1200", "tax_rate": 0, "total_amount": "1200", "total_discount_amount": 0, "total_tax_amount": 0}]}


        url = 'https://api.playground.klarna.com/checkout/v3/orders'
        response = requests.post(url, json=post_data, auth=HTTPBasicAuth('PK14387_7c7072b4680b', 'y110mtQMslO3Jqa5'))
        data = json.loads(response.content)

        order_id = data['order_id']
        order.generated_order_id = order_id
        order.save()

        return render(request, 'seminar/checkout_script.html', {'data': data, 'post': post})



def confirm_order(request, slug):
    post = get_object_or_404(Post, slug=slug)
    group_access = False

    for g in request.user.groups.all():
        if g.name == "is_authorized":
            group_access = True

    if group_access:
        order = get_object_or_404(Order, user=request.user)
        components_for_order = get_list_or_404(Order_Component, order=order)
    
        url = 'https://api.playground.klarna.com/checkout/v3/orders/'+ order.generated_order_id
        response = requests.get(url, auth=HTTPBasicAuth('PK14387_7c7072b4680b', 'y110mtQMslO3Jqa5'))
        data = json.loads(response.content)

        order.ordered = True
        order.save()

        g = Group.objects.get(name='is_authorized') 
        g.user_set.remove(request.user)

        for order_component in components_for_order:
            if order_component.component.quantity is not None:
                order_component.component.quantity -= 1
                if order_component.component.quantity is None:
                    order_component.component.quantity = 0
                order_component.component.save()

        return render(request, 'seminar/klarna_confirmation.html', {'data': data, 'post': post, 'order': order})

#Zum Finden der IP-Adresse
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def checkout_view(request, slug):
    post = get_object_or_404(Post, slug=slug)
    order = get_object_or_404(Order, user=request.user)
    group_access = False

    for g in request.user.groups.all():
        if g.name == "is_authorized":
            group_access = True

    if group_access:
        #Kalkulation des Gesamtpreises
        components_for_order = get_list_or_404(Order_Component, order=order)
        total = 0

        for order_component in components_for_order:
            tax_amount = order_component.component.item.tax_amount * order_component.component.price / 100
            taxed_price = order_component.component.price + tax_amount
            total += taxed_price

        total = round(total, 3)

        #Erstellung eines Access Tokens
        url_token = "https://axapp.sbg.ac.at/ords/ax_app_online_zahlung/oauth/token"
        token_request_data = { "grant_type": "client_credentials"} 
        response = requests.post(url_token, data=token_request_data, auth=("JCyYTGAG6vkixKIeXR91SQ..","nSOZhmZZfYq7v9Euxhwp3A..")) 

        content = json.loads(response.content.decode("utf-8"))
        access_token = content["access_token"]

        #PIN holen
        url_pin = "https://axapp.sbg.ac.at/ords/ax_app_online_zahlung/init/getpin"
        headers = {"authorization": "bearer " + access_token}
        response = requests.get(url_pin, headers=headers)
        pin = response.json()['pin']

        ip_address = get_client_ip(request)

        url = request.build_absolute_uri()

        #Transaktion starten
        url_starttrans = "https://axapp.sbg.ac.at/ords/ax_app_online_zahlung/init/starttransaction"
        required_data = {
            "ext_pers_id": request.user.id,
            "extp_vorname": request.user.first_name,
            "extp_nachname": request.user.last_name,
            "extp_plz": request.user.postalcode,
            "extp_land": request.user.country,
            "extp_stadt": request.user.city,
            "extp_strasse": request.user.address,
            "extp_hausnummer": "",
            "extp_mail": request.user.email,
            "betrag": total,
            "zahlungszweck": 10,
            "ip_adress": ip_address,
            "pin": pin,
            "zahlungsdetails": request.user.id
        }
        response = requests.post(url_starttrans, json=required_data, headers=headers) 
        data = response.json()['zahlungsurl']

        return render(request, 'seminar/pin_display.html', {'pin': pin, 'post': post, 'url': data, 'ip_address': ip_address})

def checkout_view_fr(request, slug):
    post = get_object_or_404(Post, slug=slug)
    order = get_object_or_404(Order, user=request.user)
    group_access = False

    for g in request.user.groups.all():
        if g.name == "is_authorized":
            group_access = True

    if group_access:
        #Kalkulation des Gesamtpreises
        components_for_order = get_list_or_404(Order_Component, order=order)
        total = 0

        for order_component in components_for_order:
            tax_amount = order_component.component.item.tax_amount * order_component.component.price / 100
            taxed_price = order_component.component.price + tax_amount
            total += taxed_price

        total = round(total, 3)

        #Erstellung eines Access Tokens
        url_token = "https://axapp.sbg.ac.at/ords/ax_app_online_zahlung/oauth/token"
        token_request_data = { "grant_type": "client_credentials"} 
        response = requests.post(url_token, data=token_request_data, auth=("JCyYTGAG6vkixKIeXR91SQ..","nSOZhmZZfYq7v9Euxhwp3A..")) 

        content = json.loads(response.content.decode("utf-8"))
        access_token = content["access_token"]

        #PIN holen
        url_pin = "https://axapp.sbg.ac.at/ords/ax_app_online_zahlung/init/getpin"
        headers = {"authorization": "bearer " + access_token}
        response = requests.get(url_pin, headers=headers)
        pin = response.json()['pin']

        ip_address = get_client_ip(request)

        url = request.build_absolute_uri()

        #Transaktion starten
        url_starttrans = "https://axapp.sbg.ac.at/ords/ax_app_online_zahlung/init/starttransaction"
        required_data = {
            "ext_pers_id": request.user.id,
            "extp_vorname": request.user.first_name,
            "extp_nachname": request.user.last_name,
            "extp_plz": request.user.postalcode,
            "extp_land": request.user.country,
            "extp_stadt": request.user.city,
            "extp_strasse": request.user.address,
            "extp_hausnummer": "",
            "extp_mail": request.user.email,
            "betrag": total,
            "zahlungszweck": 10,
            "ip_adress": ip_address,
            "pin": pin,
            "zahlungsdetails": request.user.id
        }
        response = requests.post(url_starttrans, json=required_data, headers=headers) 
        data = response.json()['zahlungsurl']

        return render(request, 'seminar/pin_display_fr.html', {'pin': pin, 'post': post, 'url': data, 'ip_address': ip_address})

def checkout_view_en(request, slug):
    post = get_object_or_404(Post, slug=slug)
    order = get_object_or_404(Order, user=request.user)
    group_access = False

    for g in request.user.groups.all():
        if g.name == "is_authorized":
            group_access = True

    if group_access:
        #Kalkulation des Gesamtpreises
        components_for_order = get_list_or_404(Order_Component, order=order)
        total = 0

        for order_component in components_for_order:
            tax_amount = order_component.component.item.tax_amount * order_component.component.price / 100
            taxed_price = order_component.component.price + tax_amount
            total += taxed_price

        total = round(total, 3)

        #Erstellung eines Access Tokens
        url_token = "https://axapp.sbg.ac.at/ords/ax_app_online_zahlung/oauth/token"
        token_request_data = { "grant_type": "client_credentials"} 
        response = requests.post(url_token, data=token_request_data, auth=("JCyYTGAG6vkixKIeXR91SQ..","nSOZhmZZfYq7v9Euxhwp3A..")) 

        content = json.loads(response.content.decode("utf-8"))
        access_token = content["access_token"]

        #PIN holen
        url_pin = "https://axapp.sbg.ac.at/ords/ax_app_online_zahlung/init/getpin"
        headers = {"authorization": "bearer " + access_token}
        response = requests.get(url_pin, headers=headers)
        pin = response.json()['pin']

        ip_address = get_client_ip(request)

        url = request.build_absolute_uri()

        #Transaktion starten
        url_starttrans = "https://axapp.sbg.ac.at/ords/ax_app_online_zahlung/init/starttransaction"
        required_data = {
            "ext_pers_id": request.user.id,
            "extp_vorname": request.user.first_name,
            "extp_nachname": request.user.last_name,
            "extp_plz": request.user.postalcode,
            "extp_land": request.user.country,
            "extp_stadt": request.user.city,
            "extp_strasse": request.user.address,
            "extp_hausnummer": "",
            "extp_mail": request.user.email,
            "betrag": total,
            "zahlungszweck": 10,
            "ip_adress": ip_address,
            "pin": pin,
            "zahlungsdetails": request.user.id
        }
        response = requests.post(url_starttrans, json=required_data, headers=headers) 
        data = response.json()['zahlungsurl']

        return render(request, 'seminar/pin_display_en.html', {'pin': pin, 'post': post, 'url': data, 'ip_address': ip_address})




@csrf_exempt
def payment_complete(request, slug):
    group_access = False

    iv_base64 = request.headers.get('X-Initialization-Vector')
    base64_bytes = codecs.decode(iv_base64, 'hex')
    message_iv_bytes = base64.b64decode(base64_bytes)

    details_base64 = request.headers.get('X-Zahlungsdetails')
    base64_bytes = codecs.decode(details_base64, 'hex')
    message_bytes = base64.b64decode(base64_bytes)
    details = message_bytes.decode('latin-1')

    key = "FED9B68C2D057A0F8E130F9196E1A3EB"

    aes = AES.new(key, AES.MODE_CBC, message_iv_bytes)
    
    body_bytes = base64.b64decode(request.body)
    decrypted_body = aes.decrypt(body_bytes)
    
    my_json = decrypted_body.decode('utf8').strip()
    print(my_json)
    data = json.loads(my_json)

    if data['result'] == "SUCCESS":#if data['result'] != "PENDING" and data['result'] != "NOSUCCESS":
        print("succeeded")
        user = get_object_or_404(User, id=details)

        for g in request.user.groups.all():
            if g.name == "is_authorized":
                group_access = True
                
        if group_access:
            order = get_object_or_404(Order, user=user)
            components_for_order = get_list_or_404(Order_Component, order=order)
        
            order.ordered = True
            order.generated_order_id = data['transactionID']
            order.save()

            g = Group.objects.get(name='is_authorized') 
            g.user_set.remove(user)

            for order_component in components_for_order:
                if order_component.component.quantity is not None:
                    if order_component.component.quantity == 0:
                        pass
                    else:
                        order_component.component.quantity -= 1
                else:
                    order_component.component.quantity = 0
                order_component.component.save()
    if data['result'] == "PENDING":
        print("pending")
    else:
        print("nosuccess")
    return render(request, 'seminar/payment_complete.html')
    
def error(request):
    return render(request, 'seminar/error.html')

def success(request):
    return render(request, 'seminar/success.html')


def export_csv(request):
    access = False
    for g in request.user.groups.all():
            if g.name == "csv_authorized":
                access = True
    response = HttpResponse(content_type='application/text')
    if access:
        response = HttpResponse(content_type='application/ms-excel')
        response['Content-Disposition'] = 'attachment; filename="users.xls"'

        wb = xlwt.Workbook(encoding='utf-8')
        ws = wb.add_sheet('Users')

        confirmed_orders = get_list_or_404(Order, ordered=True)

        font_style = xlwt.XFStyle()
        font_style.font.bold = True

        columns = ['User', 'Item', 'Component', 'Price']

        for col_num in range(len(columns)):
            ws.write(0, col_num+1, columns[col_num], font_style)

        font_style = xlwt.XFStyle()

        row_num = 0
        col_num = 0

        for row in confirmed_orders:
            row_num += 1
            ws.write(row_num, 1, row.user.first_name+" "+row.user.last_name, font_style)
            rows2 = get_list_or_404(Order_Component, order=row)
            for rows in rows2:
                row_num += 1
                ws.write(row_num, 2, rows.component.item.name, font_style)
                ws.write(row_num, 3, rows.component.name, font_style)
                ws.write(row_num, 4, rows.component.price, font_style)
            ws.write(row_num + 1, 0, "", font_style)
            row_num += 1

        wb.save(response)
        return response
    else:
        return response
