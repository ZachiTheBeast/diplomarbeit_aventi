from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.utils.text import slugify
from django.conf import settings
from django_countries.fields import CountryField
from django.shortcuts import render, get_object_or_404, redirect, get_list_or_404

class Post(models.Model):
    title = models.CharField(max_length = 255)
    title_en = models.CharField(max_length = 255, blank=True)
    title_fr = models.CharField(max_length = 255, blank=True)
    homepage_image = models.ImageField(upload_to="media/home")
    homepage_content = models.TextField()
    homepage_english_content = models.TextField(null=True)
    homepage_french_content = models.TextField(null=True)
    date_posted = models.DateTimeField(default = timezone.now)
    program_days = models.ManyToManyField('ProgramDay',  through='Post_ProgramDay')
    slug = models.SlugField (
    verbose_name = "Slug", allow_unicode = True, 
    unique=True, blank = True, null = True)

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.slug: self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)

    #def check_if_already_ordered(self):
        #return reverse("order-check", kwargs={'slug': self.slug})
    
    class Meta:
        verbose_name_plural = "        Post"


class Description(models.Model):
    related_post = models.OneToOneField(Post, on_delete=models.CASCADE, primary_key=True,)
    content = models.TextField()
    english_content = models.TextField(null=True)
    french_content = models.TextField(null=True)
    image = models.ImageField(upload_to="media/description", blank=True)

    def __str__(self):
        return "Description for "+self.related_post.title

    class Meta:
        verbose_name_plural = "       Description"
        
class ProgramDay(models.Model):
    posts = models.ManyToManyField('Post', through='Post_ProgramDay')
    day = models.CharField(max_length=50)
    day_en = models.CharField(max_length=50)
    day_fr = models.CharField(max_length=50)
    content = models.TextField()
    english_content = models.TextField(null=True)
    french_content = models.TextField(null=True)
    image = models.ImageField(upload_to="media/ProgramDay", blank=True)
    image_source = models.CharField(max_length=250)
    #

    def __str__(self):
        return f"{self.day}"

    class Meta:
        verbose_name_plural = "      ProgramDay"

#class Image_for_description(models.Model):
#    related_description = models.OneToOneField(Description,on_delete=models.CASCADE,primary_key=True)
#    image = models.ImageField(upload_to="media/description")
#    photo_credit = models.TextField()



class Arrival(models.Model):
    related_post = models.OneToOneField(Post,on_delete=models.CASCADE,primary_key=True)
    content = models.TextField(null=True)
    english_content = models.TextField(null=True)
    french_content = models.TextField(null=True)
    image = models.ImageField(upload_to="media/arrival", blank=True)

    def __str__(self):
        return self.related_post.title

    class Meta:
        verbose_name_plural = "    Arrival"

class Price(models.Model):
    related_post = models.OneToOneField(Post,on_delete=models.CASCADE,primary_key=True)
    content = models.TextField()
    english_content = models.TextField(null=True)
    french_content = models.TextField(null=True)

    def __str__(self):
        return self.related_post.title

    class Meta:
        verbose_name_plural = "   Price"

class Registration(models.Model):
    related_post = models.OneToOneField(Post,on_delete=models.CASCADE,primary_key=True)
    content = models.TextField()
    english_content = models.TextField(null=True)
    french_content = models.TextField(null=True)
    image = models.ImageField(upload_to="media/registration", blank=True)

    def __str__(self):
        return self.related_post.title

    class Meta:
        verbose_name_plural = "   Registration"


class Item(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    name = models.CharField(default="", max_length=255)
    name_en = models.CharField(default="", max_length=255)
    name_fr = models.CharField(default="", max_length=255)
    tax_amount = models.FloatField(default=20.0)
    is_required = models.BooleanField(default=True)
    sequence = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.post.title}, {self.name}"

    class Meta:
        verbose_name_plural = "  Items"

class Component(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    orders = models.ManyToManyField('Order', through='Order_Component')
    name = models.CharField(default="", max_length=255)
    name_en = models.CharField(default="", max_length=255)
    name_fr = models.CharField(default="", max_length=255)
    price = models.FloatField(default=0)
    quantity = models.IntegerField(null=True)
    sequence = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.item.post.title}, {self.item.name}, {self.name}"
    
    def calculateFullPrice(self):
        price_taxes = self.item.tax_amount * self.price / 100
        price_with_taxes = price_taxes + self.price

        return price_with_taxes

    class Meta:
        verbose_name_plural = " Component"

class Order(models.Model):
    generated_order_id = models.CharField(default="", max_length=255)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    ordered = models.BooleanField(default=False)
    components = models.ManyToManyField('Component',  through='Order_Component')
    ordered_date = models.DateTimeField()

    def __str__(self):
        return f"Ordered by {self.user.username}"

    def get_total(self):
        total = 0
        
        order_components = get_list_or_404(Order_Component, order=self)
        for order_component in order_components:
            price_taxes = order_component.component.item.tax_amount * order_component.component.price / 100
            price_with_taxes = price_taxes + order_component.component.price
            total += price_with_taxes
        total = round(total, 3)
        return total

class Order_Component(models.Model):
    component = models.ForeignKey('Component', on_delete=models.CASCADE)
    order = models.ForeignKey('Order', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.order.user.username} ordered {self.component.name}"

class Post_ProgramDay(models.Model):
    post = models.ForeignKey('Post', on_delete=models.CASCADE)
    programday = models.ForeignKey('ProgramDay', on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.post.title} has {self.programday.day}"

    class Meta:
        verbose_name_plural = "     Post_ProgramDay"

