from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.home_view, name='seminar-home'),
    path('de/', views.home_view, name='seminar-home'),
    path('fr/', views.home_fr_view, name='seminar-home-fr'),
    path('en/', views.home_en_view, name='seminar-home-en'),

    path('de/about/', views.about_view, name='seminar-about'),
    path('fr/about/', views.about_view_fr, name='seminar-about-fr'),
    path('en/about/', views.about_view_en, name='seminar-about-en'),

    path('de/imprint/', views.imprint_view, name='seminar-imprint'),
    path('fr/imprint/', views.imprint_view_en, name='seminar-imprint-fr'),
    path('en/imprint/', views.imprint_view_en, name='seminar-imprint-en'),

    path('de/privacypolicy/', views.privacypolicy_view, name='seminar-privacypolicy'),
    path('fr/privacypolicy/', views.privacypolicy_view_fr, name='seminar-privacypolicy-fr'),
    path('en/privacypolicy/', views.privacypolicy_view_en, name='seminar-privacypolicy-en'),

    path('de/contact/', views.contact_view, name='seminar-contact'),
    path('fr/contact/', views.contact_view, name='seminar-contact-fr'),
    path('en/contact/', views.contact_view, name='seminar-contact-en'),

    path('de/<slug:slug>/form/', views.form_view, name='seminar-form'),
    path('fr/<slug:slug>/form/', views.form_view_fr, name='seminar-form-fr'),
    path('en/<slug:slug>/form/', views.form_view_en, name='seminar-form-en'),

    path('de/<slug:slug>/', views.post_detail_view, name='post-slug'),
    path('fr/<slug:slug>/', views.post_detail_view_fr),
    path('en/<slug:slug>/', views.post_detail_view_en),

    path('<slug:slug>/ordersummary/', views.order_summary_view, name='ordersummary'),

    path('de/<slug:slug>/ordersummary/data', views.order_summary_view_data, name='ordersummary-data'),
    path('fr/<slug:slug>/ordersummary/data', views.order_summary_view_data_fr, name='ordersummary-data-fr'),
    path('en/<slug:slug>/ordersummary/data', views.order_summary_view_data_en, name='ordersummary-data-en'),

    path('<slug:slug>/payment_complete/', views.payment_complete, name='payment_complete'),
    #path('fr/<slug:slug>/payment_complete/', views.payment_complete, name='payment_complete-fr'),
    #path('en/<slug:slug>/payment_complete/', views.payment_complete, name='payment_complete-en'),

    path('de/<slug:slug>/checkout', views.checkout_view, name='checkout'),
    path('fr/<slug:slug>/checkout', views.checkout_view_fr, name='checkout-fr'),
    path('en/<slug:slug>/checkout', views.checkout_view_en, name='checkout-en'),

    path('error', views.error, name='error'),
    path('success', views.success, name='success'),

    path('<slug:slug>/confirmation/', views.confirm_order, name='confirmation'),

    path('csv', views.export_csv, name='test')
]
