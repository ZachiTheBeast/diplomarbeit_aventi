from django.shortcuts import render, get_object_or_404, redirect, get_list_or_404, render_to_response
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import RegisterForm
from .models import CustomUser
from seminar.models import Order, Order_Component, Item, Component


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            #username = form.cleaned_data.get('username')
            messages.success(request, f'Your account has been created! You can now log in!')
            return redirect('login')
    else:
        form = RegisterForm()
    return render(request, 'users/register.html', {'form': form})


@login_required
def profile(request):
    try:
        orders = get_list_or_404(Order, user=request.user).filter(ordered = True)
    except:
        orders = 0
    return render(request, 'users/profile.html', {'orders': orders})

@login_required
def profile_en(request):
    try:
        orders = get_list_or_404(Order, user=request.user).filter(ordered = True)
    except:
        orders = 0
    return render(request, 'users/profile_en.html', {'orders': orders})

@login_required
def profile_fr(request):
    try:
        orders = get_list_or_404(Order, user=request.user).filter(ordered = True)
    except:
        orders = 0
    return render(request, 'users/profile_fr.html', {'orders': orders})


    