from django.db import models
from django.contrib.auth.models import AbstractUser

class CustomUser(AbstractUser):
    pass
    first_name = models.CharField(default='', max_length=100)
    last_name = models.CharField(default='', max_length=100)
    institution = models.CharField(default='', max_length=100)
    country = models.CharField(default='', max_length=100)
    postalcode = models.CharField(default='', max_length=4)
    city = models.CharField(default='', max_length=100)
    address = models.CharField(default='', max_length=100)
    AGB = models.BooleanField(default=False, help_text="By registering you agree to our terms of service. Our Privacy Policy explains how we collect, use and share your information.")
    authorised_by_Institution = models.BooleanField(default=False, help_text="By accepting you confirm that you have been authorised by your institution.")
    
    def __str__(self):
        return self.username