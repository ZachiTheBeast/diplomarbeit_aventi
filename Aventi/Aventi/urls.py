"""Aventi URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include
from users import views as user_views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('de/register/', user_views.register, name='register'),
    path('fr/register/', user_views.register, name='register-fr'),
    path('en/register/', user_views.register, name='register-en'),

    path('de/profile/', user_views.profile, name='profile'),
    path('fr/profile/', user_views.profile_fr, name='profile-fr'),
    path('en/profile/', user_views.profile_en, name='profile-en'),

    path('de/login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path('fr/login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login-fr'),
    path('en/login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login-en'),

    path('de/logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
    path('fr/logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout-fr'),
    path('en/logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout-en'),

    path('de/password-reset/', auth_views.PasswordResetView.as_view(template_name='users/password_reset.html'), name='password_reset'),
    path('fr/password-reset/', auth_views.PasswordResetView.as_view(template_name='users/password_reset.html'), name='password_reset-fr'),
    path('en/password-reset/', auth_views.PasswordResetView.as_view(template_name='users/password_reset.html'), name='password_reset-en'),

    path('de/password-reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='users/password_reset_done.html'), name='password_reset_done'),
    path('fr/password-reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='users/password_reset_done.html'), name='password_reset_done'),
    path('en/password-reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='users/password_reset_done.html'), name='password_reset_done'),

    path('de/password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='users/password_reset_confirm.html'), name='password_reset_confirm'),
    path('fr/password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='users/password_reset_confirm.html'), name='password_reset_confirm-fr'),
    path('en/password-reset-confirm/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(template_name='users/password_reset_confirm.html'), name='password_reset_confirm-en'),

    path('de/password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(template_name='users/password_reset_complete.html'), name='password_reset_complete'),
    path('fr/password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(template_name='users/password_reset_complete.html'), name='password_reset_complete'),
    path('en/password-reset-complete/', auth_views.PasswordResetCompleteView.as_view(template_name='users/password_reset_complete.html'), name='password_reset_complete'),

    path('', include('seminar.urls'))
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
